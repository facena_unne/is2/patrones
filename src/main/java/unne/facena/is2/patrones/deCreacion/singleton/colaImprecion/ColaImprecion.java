/*
 * Implementa el Patron (Único) Singleton.
 * Proposito: Grarantiza que una clase sólo tenga una instancia, y 
 * proporciona un punto de acceso global a ella.
    
 */
package unne.facena.is2.patrones.deCreacion.singleton.colaImprecion;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public final class ColaImprecion {
    
    //identificador de proceso ;
    private int pid;
    //paginas en la cola de imprecion.
    private ArrayList<Documento> documentos;    
    //contiene la cola unica instancia de cola de impreicon
    private static ColaImprecion cola;
    
    public ColaImprecion(){}
    
    public ColaImprecion(int p_pid)
    {
        this.setPid(p_pid);
        this.setDocumentos( new ArrayList<>() );        
    }
  
    public static ColaImprecion getInstance(int p_pid) {
        //si no tenemos una cola de imprecion instanciada        
        if (ColaImprecion.getCola()==null){
            //instanciamos una nueva cola de imprecion.
            cola = new ColaImprecion(p_pid);
        }
        
        return cola;
    }
    
 
    public static void documentosEnCola()
    {
        //TODO: remover el Code smell :)
        Iterator<Documento> _itr = ColaImprecion.cola
            .getDocumentos()
            .iterator();
        //END Code smell------------ :(
        System.out.println("Documento en la cola de impresion");
        System.out.println("PID::: " + ColaImprecion.getInstance(89).getPid());
        int _indice=1;
        while(_itr.hasNext()){            
            Documento _doc = _itr.next();
            System.out.println("["+(_indice++) + "] - " + _doc.toString());
        }        
        System.out.println(
            ColaImprecion.cola.getDocumentos().size() 
            +  " documentos en cola de impresion."
        );
    }
     
    
    public boolean agregarAcola(Documento p_documento){
        ColaImprecion _cola = ColaImprecion.getInstance(0);
        return _cola.getDocumentos().add(p_documento);
    }
    
    public static ColaImprecion getCola() {
        return cola;
    }
           
    public static void setCola(ColaImprecion aCola) {
        cola = aCola;
    }

    public ArrayList<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(ArrayList<Documento> documentos) {
        this.documentos = documentos;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }
    
    
}
