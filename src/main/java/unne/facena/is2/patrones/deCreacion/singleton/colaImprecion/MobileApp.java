/*
 * Utilizamos esta clase para simular un usuario del la cola de imprecion.
 */
package unne.facena.is2.patrones.deCreacion.singleton.colaImprecion;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class MobileApp {
    
    private String nombreDispositivo;
    private String sistemaOperativo;
    
    public MobileApp(){}
    
    public MobileApp(String p_nombreDispositivo, String p_sistemaOperativo){
        this.setNombreDispositivo(p_nombreDispositivo);
        this.setSistemaOperativo(p_sistemaOperativo);        
    }
    public boolean imprimirDocumento(String p_nombre){
        //instanciamos un documento.
        Documento _documento = new Documento(
            this.getNombreDispositivo(),
            p_nombre
        );
        //Accesamos a la instancia de la cola de imprecion.
        ColaImprecion _cola = ColaImprecion.getInstance(99);
        //agregamos nuestro documento.
        return _cola.agregarAcola(_documento);
    }

    public String getNombreDispositivo() {
        return nombreDispositivo;
    }

    public void setNombreDispositivo(String nombreDispositivo) {
        this.nombreDispositivo = nombreDispositivo;
    }

    public String getSistemaOperativo() {
        return sistemaOperativo;
    }

    public void setSistemaOperativo(String sistemaOperativo) {
        this.sistemaOperativo = sistemaOperativo;
    }
    
    
    
}
