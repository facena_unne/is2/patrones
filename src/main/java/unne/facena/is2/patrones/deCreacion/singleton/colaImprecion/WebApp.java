/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unne.facena.is2.patrones.deCreacion.singleton.colaImprecion;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class WebApp {
    private String serverName;
    private String appName;
    
    public WebApp(){}
    public WebApp(String p_serverName, String p_appName){
        
        this.setAppName(p_appName);
        this.setServerName(p_serverName);
    
    }
    
     public boolean imprimirDocumento(String p_nombreDocumento){
        //instanciamos un documento.
        Documento _documento = new Documento(
            this.getAppName(),
            p_nombreDocumento
        );
        //Accesamos a la instancia de la cola de imprecion.
        ColaImprecion _cola = ColaImprecion.getInstance(99);
        //agregamos nuestro documento.
        return _cola.agregarAcola(_documento);
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }
    
    
    
}
