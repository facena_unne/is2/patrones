/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unne.facena.is2.patrones.deCreacion.singleton.colaImprecion;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Documento {
    
    private String origen;    
    private String nombre;
    private int pagina;    
    
    public Documento(){}
    public Documento(String p_origen, String p_nombre){
        this.setOrigen(p_origen);
        this.setNombre(p_nombre);
        this.setPagina(1);        
    }
    public Documento(String p_origen, String p_nombre,int p_pagina){
        this.setOrigen(p_origen);
        this.setNombre(p_nombre);
        this.setPagina(p_pagina);        
    }
    
    public String toString(){
        return 
            "Origen: " +this.getOrigen() +"\r"               
            +"Nombre: " 
            + this.getNombre() 
            + ", paginas: " + this.getPagina()+".";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
    
    
    
}
