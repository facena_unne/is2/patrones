/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unne.facena.is2.patrones.estructural.decorador.archivo;

import java.util.Base64;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Encriptado extends ArchivoDecorable{
    
    public Encriptado(Envoltorio p_archivoBasico) {
        super(p_archivoBasico);
    }

    @Override
    public void escrivir(String data) {
        super.escrivir(encode(data));
    }

    @Override
    public String leer() {
        return decode(super.leer());
    }

    private String encode(String data) {
        byte[] result = data.getBytes();
        for (int i = 0; i < result.length; i++) {
            result[i] += (byte) 1;
        }
        return Base64.getEncoder().encodeToString(result);
    }

    private String decode(String data) {
        byte[] result = Base64.getDecoder().decode(data);
        for (int i = 0; i < result.length; i++) {
            result[i] -= (byte) 1;
        }
        return new String(result);
    }
    
}
