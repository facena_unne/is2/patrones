/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unne.facena.is2.patrones.estructural.decorador.archivo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class Comprimido extends ArchivoDecorable{
    
    private int nivelCompresion = 6;

    public Comprimido(Envoltorio p_archivoBasico) {
        super(p_archivoBasico);
    }

   
    @Override
    public void escrivir(String p_datos) {
        super.escrivir(this.comprimir(p_datos));
    }

    @Override
    public String leer() {
        return this.descomprimir(super.leer());
    }

    private String comprimir(String stringData) {
        byte[] data = stringData.getBytes();
        try {
            ByteArrayOutputStream bout = new ByteArrayOutputStream(512);
            DeflaterOutputStream dos = new DeflaterOutputStream(bout, new Deflater(this.getNivelCompresion()));
            dos.write(data);
            dos.close();
            bout.close();
            return Base64.getEncoder().encodeToString(bout.toByteArray());
        } catch (IOException ex) {
            return null;
        }
    }

    private String descomprimir(String stringData) {
        byte[] data = Base64.getDecoder().decode(stringData);
        try {
            InputStream in = new ByteArrayInputStream(data);
            InflaterInputStream iin = new InflaterInputStream(in);
            ByteArrayOutputStream bout = new ByteArrayOutputStream(512);
            int b;
            while ((b = iin.read()) != -1) {
                bout.write(b);
            }
            in.close();
            iin.close();
            bout.close();
            return new String(bout.toByteArray());
        } catch (IOException ex) {
            return null;
        }
    }

    public int getNivelCompresion() {
        return nivelCompresion;
    }

    public void setNivelCompresion(int nivelCompresion) {
        this.nivelCompresion = nivelCompresion;
    }
    
    
}
