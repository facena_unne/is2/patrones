/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unne.facena.is2.patrones.estructural.decorador.archivo;

/**
 *
 * @author Pablo N. Garcia Solanellas
 * Envoltorio o Wrapper
 */
public interface Envoltorio {
    void escrivir(String datos);

    String leer();  
    
}
