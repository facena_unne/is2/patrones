/*
 * Esta clase modela el comportamiento de un archivo plano.
 * a los efectos del patron Decorador una instancia de esta clase 
 * reprecenta el objeto concreto.
 */
package unne.facena.is2.patrones.estructural.decorador.archivo;

import java.io.*;
/**
 *
 * @author Pablo N. Garcia Solanellas 
 */

public class ArchivoPlano implements Envoltorio{
    private String nombre;
     
    public ArchivoPlano(String p_nombre){
        this.setNombre(p_nombre);
    } 
            
    @Override
    public void escrivir(String p_datos) {
        File file = new File(this.getNombre());
        
        try (OutputStream fos = new FileOutputStream(file)) {
            fos.write(p_datos.getBytes(), 0, p_datos.length());
        } catch (IOException ex) {
            //TODO: remover el Code smell :)
            System.out.println(ex.getMessage());            
            //END Code smell :(
        }
    }
    
    @Override
    public String leer() {
        char[] buffer = null;
        File file = new File(this.getNombre());
        try (FileReader reader = new FileReader(file)) {
            buffer = new char[(int) file.length()];
            reader.read(buffer);
        } catch (IOException ex) {
            //TODO: remover el Code smell :)
            System.out.println(ex.getMessage());
            //END Code smell :(
        }
        return new String(buffer);
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
     
     
     
}
