/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unne.facena.is2.patrones.estructural.decorador.archivo;

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class ArchivoDecorable implements Envoltorio{
    //wrapper
    private Envoltorio envoltorio;
    
    public ArchivoDecorable(Envoltorio p_envoltorio){
        this.setEnvoltorio(p_envoltorio);
    }
    
    @Override
    public void escrivir(String p_datos) {
        this.getEnvoltorio().escrivir(p_datos);
    }
    
    @Override
    public String leer() {
        return this.getEnvoltorio().leer();
    }
    
    public Envoltorio getEnvoltorio() {
        return envoltorio;
    }

    public void setEnvoltorio(Envoltorio envoltorio) {
        this.envoltorio = envoltorio;
    }
    
}
