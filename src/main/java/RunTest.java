
import unne.facena.is2.patrones.deCreacion.singleton.colaImprecion.ColaImprecion;
import unne.facena.is2.patrones.deCreacion.singleton.colaImprecion.MobileApp;
import unne.facena.is2.patrones.deCreacion.singleton.colaImprecion.WebApp;
import unne.facena.is2.patrones.estructural.decorador.archivo.ArchivoPlano;
import unne.facena.is2.patrones.estructural.decorador.archivo.ArchivoDecorable;
import unne.facena.is2.patrones.estructural.decorador.archivo.Comprimido;
import unne.facena.is2.patrones.estructural.decorador.archivo.Encriptado;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Pablo N. Garcia Solanellas
 */
public class RunTest {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println( "Proyecto de ejemplo de Patrones de Diseño" );
        System.out.println( "IS2 FaCENA, UNNE Corrientes, Argentina.");
        
        /*Patron UNICO (Singleton)*/        
        //RunTest.ejemploColaImprecion();
        //Descomente la line anterior para ver el ejemplo de Singleton
        
        /*Decorador*/
        RunTest.ejemploDecoradorDeArchivos();
    }
    
    private static void ejemploColaImprecion(){
        System.out.println( "Singleton");        
        //Definimos dos Aplicaciones web
        WebApp _web1 = new WebApp("web1", "Apache");        
        WebApp _web2 = new WebApp("web2", "Nginx");
        
        MobileApp _mobile11 = new MobileApp("iPad","IOS");
        MobileApp _mobile12 = new MobileApp("A52","Android");
        
        _web1.imprimirDocumento("Factura de compra.pdf");
        _web2.imprimirDocumento("Lista de usaurios.xls");
        
        _mobile11.imprimirDocumento("Resumen banco.pdf");
        _mobile11.imprimirDocumento("doc sin nombre.pdf");
        
        _mobile12.imprimirDocumento("documento.pdf");
        
        //veamos el contenido de la cola de imprecion.
        System.out.println("");
        ColaImprecion.documentosEnCola();
    }
    private static void ejemploDecoradorDeArchivos(){
        String _docentes = "Maria Ferraro, Laura Gomes, Cristian Benites";
        System.out.println("- Informacion para agregar al archivo ----------------");
        System.out.println(_docentes);
        
        ArchivoDecorable _archivo = new Comprimido(
                                         new Encriptado(
                                             new ArchivoPlano("IS2_2021.txt")));
        _archivo.escrivir(_docentes);
        ArchivoPlano plain = new ArchivoPlano("IS2_2021.txt");

     
        System.out.println("- Encriptado --------------");
        System.out.println(plain.leer());
        System.out.println("- DES-Encriptado --------------");
        System.out.println(_archivo.leer());
    }
            
}
